'use strict';

var app = angular.module('myApp', ['ui.router', 'ui.bootstrap']);

app.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: '/partials/home.html',
            controller: 'HomeCtrl',
            controllerAs: 'home'
        })
        .state('test', {
            url: '/test',
            templateUrl: '/partials/test.html'
        })

        // render a component from a state
        .state('foo', {
            url: '/foo',
            component: 'fooComponent',
            resolve: {
                simpleObj: function(){
                    return {value: 'simple!'};
                },
                fooData: function(dataFactory) {
                    return dataFactory;
                }
            }
        })
        .state('bar', {
            url: '/bar',
            component: 'barComponent'
        });
});