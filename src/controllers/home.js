app.controller('HomeCtrl', function($http, dataFactory) {
    var home = this;
    console.log('hi from home!');

    dataFactory
        .then(function(response) {
            home.ip = response.data.ip;
        },
        function(response) {
            console.log('Error getting response!');
        });

    $http
        .get('http://headers.jsontest.com')
        .then(function(response) {
            home.headers = response;
        },
        function(response) {
        })
});