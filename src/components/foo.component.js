// Component syntax new to Angular 1.5

app.component('fooComponent', {
    bindings: {
        data: '@' // pass string to component as attribute
    },
    template: `
      <div class="panel panel-default">
          <div class="panel-heading">Test from foo component</div>  
          <div class="panel-body">
            <p>{{ $ctrl.data  }}</p>
          </div>
      </div>
    `
    ,
    controller: function () {
        this.message = "hi from foo component!";
        // console.log(this.fooData.data);
    }
});