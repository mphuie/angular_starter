// Component syntax new to Angular 1.5

app.component('barComponent', {
    bindings: {
        somedata: '<' // pass string to component as attribute
    },
    template: `
      <div class="panel panel-default">
          <div class="panel-heading">Test from bar component</div>  
          <div class="panel-body">I got this from the parent controller {{ $ctrl.somedata }} 
          <p>I got this from my controller {{ $ctrl.message }}</p>
          </div>
      </div>
    `
    ,
    controller: function() {
        var ctrl = this;

        console.log('hi from barComponent controller')
        ctrl.message = "hi there!";
    }
});