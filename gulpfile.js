var gulp = require('gulp');
var concat = require('gulp-concat');
var gls = require('gulp-live-server');

gulp.task('serve', function() {
    var server = gls.static(['public']);
    server.start();

    gulp.watch(['public/**/*.css', 'public/**/*.html', 'src/**/*.js'], function (file) {
        gulp.src([
            './src/app.module.js',
            './src/app.factories.js',
            './src/controllers/*.js',
            './src/components/*.js'
        ])
            .pipe(concat('app.js'))
            .pipe(gulp.dest('./public/'));
        server.notify.apply(server, [file]);
    });
});

gulp.task('concat', function() {
    return gulp.src([
        './src/app.module.js',
        './src/app.factories.js',
        './src/controllers/*.js',
        './src/components/*.js'
        ])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./public/'));
});

